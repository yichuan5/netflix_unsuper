# README #

This project use mixture model for collaborative filtering of users' Netflix movie rating

### Files ###

*kmeans.py baseline using the K-means algorithm

*em.py mixture model and fill matrix function

*common.py common functions

*main.py run experiments

*test.py EM test case