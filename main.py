import numpy as np
import kmeans
import common
import em

X = np.loadtxt("toy_data.txt")

def run_kmeans():
    for K in range(1,5):
        min_cost=None
        best_seed=None
        for seed in range(0,5):
            mixture, post = common.init(X, K, seed)
            mixture, post, cost = kmeans.run(X, mixture, post)
            if min_cost is None or cost < min_cost:
                min_cost = cost
                best_seed= seed
                
        mixture, post = common.init(X, K, best_seed)
        mixture, post, cost = kmeans.run(X, mixture, post)
        title = "K-means for K ={}, seed={}, cost={}".format(K, best_seed, min_cost)
        print(title)
        common.plot(X, mixture, post, title)
        
        
def run_em():
    for K in range(1,5):
        min_cost=None
        best_seed=None
        for seed in range(0,5):
            mixture, post = common.init(X, K, seed)
            mixture, post, cost = em.run(X, mixture, post)
            if min_cost is None or cost < min_cost:
                min_cost = cost
                best_seed= seed
                
        mixture, post = common.init(X, K, best_seed)
        mixture, post, cost = em.run(X, mixture, post)
        title = "EM for K ={}, seed={}, cost={}".format(K, best_seed, min_cost)
        print(title)
        common.plot(X, mixture, post, title)

        
X = np.loadtxt("netflix_incomplete.txt")
seed=1
n, d = X.shape
X_pred = X.copy()
K, _ = mixture.mu.shape

for i in range(n):
    mask = X[i, :] != 0
    mask0 = X[i, :] == 0
    post = np.zeros(K)
    for j in range(K):
        log_likelihood = log_gaussian(X[i, mask], mixture.mu[j, mask],
                                      mixture.var[j])
        post[j] = np.log(mixture.p[j]) + log_likelihood
    post = np.exp(post - logsumexp(post))
    X_pred[i, mask0] = np.dot(post, mixture.mu[:, mask0]
